#=============================================================================
#
#  Copyright (c) Kitware, Inc.
#  All rights reserved.
#  See LICENSE.txt for details.
#
#  This software is distributed WITHOUT ANY WARRANTY; without even
#  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#  PURPOSE.  See the above copyright notice for more information.
#
#=============================================================================

set (unit_tests
  UnitTestDream3DPipelineOp.cxx
  UnitTestRevolveOp.cxx
  UnitTestPartitionBoundariesOp.cxx)

set (unit_tests_which_require_data)

smtk_unit_tests(
  LABEL "MultiscaleSession"
  SOURCES ${unit_tests}
  SOURCES_REQUIRE_DATA ${unit_tests_which_require_data}
  LIBRARIES smtkCore smtkMultiscaleSession smtkMeshSession ${Boost_LIBRARIES})

smtk_get_kit_name(kit)
target_compile_definitions(UnitTests_${kit} PRIVATE "AFRL_DIR=\"${AFRL_DIR}\"")
